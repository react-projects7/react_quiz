import axios from 'axios';

export default axios.create({
    baseURL: 'https://frontend-8843f.firebaseio.com'
})